from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


def get_menu_kb() -> InlineKeyboardMarkup:
    buttons = [
        [InlineKeyboardButton(text="Метод Собела", callback_data="cb_sobel"),
         InlineKeyboardButton(text="Медианный фильтр", callback_data="cb_median")],
        [InlineKeyboardButton(text="Билатеральный фильтр", callback_data="cb_bilateral")]
    ]
    keyboard = InlineKeyboardMarkup(inline_keyboard=buttons)
    return keyboard