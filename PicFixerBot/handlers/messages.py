from aiogram import Router
from aiogram.types import Message
from aiogram.filters.command import Command
from aiogram.utils.formatting import Text, Italic
from keyboards.for_menu import get_menu_kb


router = Router()

@router.message(Command('start'))
async def cmd_start(message: Message):
    await message.answer("Привет! Меня зовут PicFixer, я - бот для обработки фотографий")
    await message.answer("Для доступа к функционалу, напишите /menu")

@router.message(Command("hello"))
async def cmd_hello(message: Message):
    content = Text(
        "Привет, ",
        Italic(message.from_user.full_name),
        " Для доступа к функционалу, напишите /menu"
    )
    await message.answer(**content.as_kwargs())

@router.message(Command("menu"))
async def get_menu(message: Message):
    await message.answer("На данный момент доступен следующий функционал",
                         reply_markup=get_menu_kb())