import os
from aiogram import Router, F
from aiogram.types import Message, CallbackQuery, FSInputFile
import methods as md


router = Router()

clbk = None
@router.callback_query(F.data)
async def filter_type(callback: CallbackQuery):
    global clbk
    clbk = callback.data[3:]

    suffix = "Отправьте изображение для обработки"

    action = callback.data[3:]
    if action == "sobel":
        await callback.message.answer("Вы выбрали <i>МЕТОД СОБЕЛА</i>. " + suffix)
    elif action == "median":
        await callback.message.answer("Вы выбрали <i>МЕДИАННЫЙ ФИЛЬТР</i>. " + suffix)
    elif action == "bilateral":
        await callback.message.answer("Вы выбрали <i>БИЛАТЕРАЛЬНЫЙ ФИЛЬТР</i>. " + suffix)

@router.message(F.photo)
async def get_photo(message: Message, bot):
    global clbk
    os.makedirs(f'files/', exist_ok=True)
    await bot.download(
        message.photo[-1],
        destination=f"files/{message.from_user.id}.jpg"
    )
    await message.answer("Ваш запрос обрабатывается... Подождите...")
    await apply_filter(clbk, message)

@router.message(F.text)
async def any_message(message: Message):
    await message.answer("Я вас не понимаю 😕. Пожалуйста, введите какую-либо <i>команду</i>")


async def apply_filter(clbk, msg):
    filePath = f"files/{msg.from_user.id}.jpg"
    if clbk == "sobel":
        md.sobel_filter(filePath)
    elif clbk == "median":
        md.median_denoise(filePath)
    elif clbk == "bilateral":
        md.bilateral_filter(filePath)
    newImage = FSInputFile(filePath)
    await msg.answer_photo(newImage, caption="Обработанное изображение 👆\nДля доступа к функционалу введите /menu")
    os.remove(filePath)