import asyncio
import logging
from aiogram import Bot, Dispatcher

from config_reader import config
from handlers import messages, different_types


async def main():
    bot = Bot(token=config.bot_token.get_secret_value(), parse_mode="HTML")
    dp = Dispatcher()

    logging.basicConfig(filename="bot_logging.log", encoding="utf-8", 
                        format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %H:%M:%S",
                        level=logging.INFO)
    # logging.basicConfig(level=logging.INFO)
    
    dp.include_routers(messages.router, different_types.router)

    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == '__main__':
    asyncio.run(main())